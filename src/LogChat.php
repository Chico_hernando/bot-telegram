<?php

include('../vendor/autoload.php');

use NlpTools\Tokenizers\WhitespaceTokenizer;
use NlpTools\Similarity\CosineSimilarity;

$tokenizer = new WhitespaceTokenizer();
$sim = new CosineSimilarity();

class LogChat
{

    public function insertLog($user_id,$text)
    {
        global $connection;
        $query = 'INSERT INTO log_chat SET user_id = '.$user_id.', text_chat = "'.$text.'"';
        if (mysqli_query($connection,$query)){
            var_dump("Berhasil menyimpan");
        } else {
            var_dump("Tidak Berhasil menyimpan");
        }

        return null;
    }

    function getWikipediaPage($page) {
        ini_set('user_agent', 'NlpToolsTest/1.0 (tests@php-nlp-tools.com)');
        $page = json_decode(file_get_contents("http://id.wikipedia.org/w/api.php?format=json&action=parse&page=".urlencode($page)),true);
        return preg_replace('/\s+/',' ',strip_tags($page['parse']['text']['*']));
    }

    public function similiar()
    {
        global $tokenizer;
        global $sim;
        $aris = $tokenizer->tokenize($this->getWikipediaPage('Aristotle'));
        $archi = $tokenizer->tokenize($this->getWikipediaPage('Archimedes'));
        $einstein = $tokenizer->tokenize($this->getWikipediaPage('Albert Einstein'));

        $soekarno = $tokenizer->tokenize($this->getWikipediaPage('Soekarno'));
        $soeharto = $tokenizer->tokenize($this->getWikipediaPage('Soeharto'));
        $a = $tokenizer->tokenize("Budi menendang bola");
        $b = $tokenizer->tokenize("bola ditendang Budi");

//        $aris_to_archi = $sim->similarity(
//            $aris,
//            $archi
//        );
//
//        $aris_to_albert = $sim->similarity(
//            $aris,
//            $einstein
//        );
//
//        var_dump($aris_to_archi,$aris_to_albert);

        $karno_to_harto = $sim->similarity(
            $soekarno,
            $soeharto
        );

        $a_to_b = $sim->similarity(
            $a,
            $b
        );

        var_dump($karno_to_harto,$a_to_b);

    }

}