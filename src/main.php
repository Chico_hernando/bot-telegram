<?php

include ("User.php");
include ("LogChat.php");
include ("Compiler.php");
include ("../connection.php");


$user = new User();
$logChat = new LogChat();
$db = new dbObj();
$compiler = new Compiler();

$connection =  $db->getConnstring();

$TOKEN      = "1124577684:AAGPUFcFSJhNzCvZp99NpHlkmw6DzORUgtE";
$usernamebot= "@utteracademybot";

$debug = false;
date_default_timezone_set('Asia/Jakarta');

function request_url($method)
{
    global $TOKEN;
    return "https://api.telegram.org/bot" . $TOKEN . "/". $method;
}

function get_updates($offset)
{
    $url = request_url("getUpdates")."?offset=".$offset;
    $resp = file_get_contents($url);
    $result = json_decode($resp, true);
    if ($result["ok"]==1)
        return $result["result"];
    return array();
}

function send_reply($chatid, $msgid, $text)
{
    global $debug;
    $data = array(
        'chat_id' => $chatid,
        'text'  => $text,
        'reply_to_message_id' => $msgid
    );

    $url = request_url('sendMessage');
    $result = url_get_content($url,$data);

    if ($debug)
        print_r($result);
}

function send_reply_without_reply($chatid, $text,$button = null)
{
    global $debug;
    if ($button){
    $keyboard = [
        'inline_keyboard' => [
            [
                ['text' => 'sumber part 1' , 'url' => 'https://www.malasngoding.com/belajar-java-intro-dan-instalasi/'],
                ['text' => 'sumber part 2' , 'url' => 'https://www.malasngoding.com/belajar-dasar-java/'],
                ['text' => 'sumber part 3' , 'url' => 'https://www.malasngoding.com/pengertian-variabel-pada-java/'],
            ],
            [
                ['text' => 'sumber part 4' , 'url' => 'https://www.malasngoding.com/tipe-data-angka-pada-java/'],
                ['text' => 'sumber part 5' , 'url' => 'https://www.malasngoding.com/tipe-data-karakter-pada-java/'],
                ['text' => 'sumber part 6' , 'url' => 'https://www.malasngoding.com/operator-matematika-dalam-java/']
            ],
            [
                ['text' => 'sumber part 7' , 'url' => 'https://www.malasngoding.com/fungsi-tipe-data-string-pada-java/'],
                ['text' => 'sumber part 8' , 'url' => 'https://www.malasngoding.com/penggunaan-if-dan-else-pada-java/'],
                ['text' => 'sumber part 9' , 'url' => 'https://www.malasngoding.com/perulangan-pada-java/']
            ],
            [
                ['text' => 'sumber part 10' , 'url' => 'https://www.malasngoding.com/pembahasan-array-pada-java/'],
                ['text' => 'sumber part 11' , 'url' => 'https://www.malasngoding.com/category/java']
            ],
            [
                ['text' => '   ','callback_data' => 'Ini pembatas']
            ],
            [
                ['text' => 'part 1' , 'callback_data' => "Apakah kalian tahu java?\nJava Adalah salah satu bahasa"],
                ['text' => 'part 2' , 'callback_data' => 'MODUL-Algoritma-dan-Pemrograman-2018'],
                ['text' => 'part 3' , 'callback_data' => 'Part 3 (Coming Soon)']
            ],
            [
                ['text' => 'part 4' , 'callback_data' => 'Part 4 (Coming Soon)'],
                ['text' => 'part 5' , 'callback_data' => 'Part 5 (Coming Soon)'],
                ['text' => 'part 6' , 'callback_data' => 'Part 6 (Coming Soon)']
            ],[
                ['text' => 'part 7' , 'callback_data' => 'Part 7 (Coming Soon)'],
                ['text' => 'part 8' , 'callback_data' => 'Part 8 (Coming Soon)'],
                ['text' => 'part 9' , 'callback_data' => 'Part 9 (Coming Soon)']
            ],
            [
                ['text' => 'part 10' , 'callback_data' => 'Part 10 (Coming Soon)'],
                ['text' => 'part 11' , 'callback_data' => 'Part 11 (Coming Soon)']
            ]
        ]
    ];
    $encodedKeyboard = json_encode($keyboard);
    $data = array(
        'chat_id' => $chatid,
        'text' => $text,
        'reply_markup' => $encodedKeyboard
    );
    }else{
        $data = array(
            'chat_id' => $chatid,
            'text' => $text
        );
    }


        $url = request_url('sendMessage');
    $result = url_get_content($url,$data);

    if ($debug)
        print_r($result);
}

function url_get_content($Url, $data)
{
    if (!function_exists('curl_init')) {
        $this->addLog('CURL is not installed!');
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function send_documents($chatId,$nama){

    $filepath = realpath('../Modul/'.$nama.'.pdf');
    $url = request_url("sendDocument");

    $post = array(
        'chat_id' => $chatId,
        'document'=>new CurlFile($filepath)
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    $output = curl_exec ($ch);
    curl_close ($ch);
    return $output;
}


function create_response($text, $message)
{
    global $usernamebot;
    global $logChat;


    $hasil = '';
    $reply = true;
    $button = false;
    $start = false;

    $fromid = $message["from"]["id"];
    $chatid = $message["chat"]["id"];
    $pesanid= $message['message_id'];

    isset($message["from"]["username"])
        ? $chatuser = $message["from"]["username"]
        : $chatuser = '';

    isset($message["from"]["last_name"])
        ? $namakedua = $message["from"]["last_name"]
        : $namakedua = '';

    $namauser = $message["from"]["first_name"]. ' ' .$namakedua;
    $textur = preg_replace('/\s\s+/', ' ', $text);
    $command = explode(' ',$textur,2); //

    switch ($command[0]) {

        case '/id':
        case '/id'.$usernamebot : //dipakai jika di grup yang haru ditambahkan @usernamebot
            $hasil = "$namauser, ID kamu adalah $fromid";
            break;

        case '/time':
        case '/time'.$usernamebot :
            $hasil  = "$namauser, waktu lokal bot sekarang adalah :\n";
            $hasil .= date("d M Y")."\nPukul ".date("H:i:s");
            break;

        case '/nama':
        case '/nama'.$usernamebot :
            $hasil = "Namamu adalah : $namauser";
            break;

        case '/java':
        case '/java'.$usernamebot :
            $hasil = "Belajar Materi java mulai dari 0";
            $reply = false;
            $button = true;
            break;

        case '/start':
        case '/start'.$usernamebot :
            $hasil = "Hai, saya adalah bot untuk belajar";
            $start = true;
        break;

        case '/similiar':
        case '/similiar'.$usernamebot :
            $hasil = "coba similiar";
            $logChat->similiar();
            $reply = false;
            break;

        case '/compiler_sederhana':
            $hasil = "Masukkan bahasa java yang ingin di compile";
            break;
    }
    return array($hasil, $reply,$button, $start);
}

function process_message($message)
{
    global $user;
    global $logChat;
    global $compiler;

    if (isset($message["message"]["chat"]["id"]) && !isset($message["message"]["entities"])){
        if ($message["message"]["chat"]["id"] > 0){
            $chatid = $message["message"]["chat"]["id"];
        $userId = $message["message"]["from"]["id"];
        $chat_text = $message["message"]["text"];
        send_reply_without_reply($chatid,"Pesan saya simpan");
        $logChat->insertLog($userId,$chat_text);
        }
    }

    if (isset($message["callback_query"]["data"])){
        $callbackQuery = $message["callback_query"]["data"];
        $updateid = $message["update_id"];
        $chatid = $message["callback_query"]["message"]["chat"]["id"];
        if ($callbackQuery == "MODUL-Algoritma-dan-Pemrograman-2018"){
            send_documents($chatid,$callbackQuery);
        } else{
        send_reply_without_reply($chatid,$callbackQuery);
        }
    } else {
        $updateid = $message["update_id"];
        $message_data = $message["message"];

        if (isset($message_data["text"])) {
            $chatid = $message_data["chat"]["id"];
            $message_id = $message_data["message_id"];
            $text = $message_data["text"];
            $response = create_response($text, $message_data);
            var_dump($response[0]);

            if (!empty($response)) {
                    if ($response[0] == "Masukkan bahasa java yang ingin di compile"){
//                        send_reply_without_reply($chatid, "Lebokno boso jowo seng pengen bok kompel");
                        send_reply_without_reply($chatid, "Ini adalah perintah untuk meng compile bahasa java");
                        $compiler->_compiler($message);
                    } else

                if (!$response[1]) {
                    send_reply_without_reply($chatid, $response[0], $response[2]);
                } else {

                    if ($response[3]){
                        $id = $message_data["from"]["id"];
                        $firstName = $message_data["from"]["first_name"];
                        $lastname = $message_data["from"]["last_name"];
                        $username = $message_data["from"]["username"];
                        $fullname = $firstName." ".$lastname;
                        $user->insertUser($id,$chatid,$fullname,$username);
                    }

                    send_reply($chatid, $message_id, $response[0]);
                }
            }
        }
    }
    return $updateid;
}

function process_one()
{
    global $debug;
    global $compiler;
    $update_id  = 0;
    echo "-";

    if (file_exists("last_update_id"))
        $update_id = (int)file_get_contents("last_update_id");

    $updates = get_updates($update_id);

    if ((!empty($updates)) and ($debug) )  {
        echo "\r\n===== isi diterima \r\n";
        print_r($updates);
    }

    foreach ($updates as $message)
    {
        echo '+';
        $compiler->_compiler($message);
        $update_id = process_message($message);
    }

    file_put_contents("last_update_id", $update_id + 1);
}

// metode poll
// proses berulang-ulang
// sampai di break secara paksa
// tekan CTRL+C jika ingin berhenti
while (true) {
    process_one();
    sleep(1);
}

// metode webhook
// secara normal, hanya bisa digunakan secara bergantian dengan polling
// aktifkan ini jika menggunakan metode webhook
/*
$entityBody = file_get_contents('php://input');
$pesanditerima = json_decode($entityBody, true);
process_message($pesanditerima);
*/

?>
